package main

import (
	"fmt"
	"net/http"

	"github.com/rizkyian78/test/src/controller"
)

func main() {

	handler, err := controller.NewHandler()
	if err != nil {
		fmt.Println(err.Error())
		panic("Failed to connect db")
	}

	http.HandleFunc("/user", handler.GetUser)
	http.HandleFunc("/create", handler.AddUser)
	http.HandleFunc("/update/", handler.UpdateUser)
	http.HandleFunc("/delete/", handler.DeleteUser)

	address := ":9000"
	fmt.Printf("server started at %s\n", address)
	err = http.ListenAndServe(address, nil)
	if err != nil {
		fmt.Println(err.Error())
		panic("Failed to start server")
	}
}
