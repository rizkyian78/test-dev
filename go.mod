module github.com/rizkyian78/test

go 1.21.1

require github.com/satori/go.uuid v1.2.0

require (
	github.com/jaswdr/faker v1.19.1 // indirect
	github.com/lib/pq v1.10.9 // indirect
)
