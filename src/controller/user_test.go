package controller_test

import (
	"bytes"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/jaswdr/faker"
	"github.com/rizkyian78/test/src/controller"
)

func TestGetUser(t *testing.T) {

	t.Run("Test Get All User", func(t *testing.T) {
		req, err := http.NewRequest("GET", "/users", nil)
		if err != nil {
			t.Fatal(err)
		}

		rr := httptest.NewRecorder()

		handler, err := controller.NewHandler()
		if err != nil {
			t.Fatal(err)
		}
		handler.GetUser(rr, req)

		if status := rr.Code; status != 200 {
			t.Errorf("handler returned wrong status code: got %v want %v", status, 200)
		}
	})

}

func TestAddUser(t *testing.T) {

	t.Run("Test Add User", func(t *testing.T) {

		fake := faker.New()
		reqBody := []byte(fmt.Sprintf(`{"username":"%s","password":"%s"}`, fake.Person().Name(), fake.Internet().Password()))
		fmt.Println(string(reqBody))
		req, err := http.NewRequest("POST", "/create", bytes.NewBuffer(reqBody))
		if err != nil {
			t.Fatal(err)
		}

		rr := httptest.NewRecorder()

		handler, err := controller.NewHandler()
		if err != nil {
			t.Fatal(err)
		}
		handler.AddUser(rr, req)

		if status := rr.Code; status != 200 {
			t.Errorf("handler returned wrong status code: got %v want %v", status, 200)
		}
	})
}

func TestUpdateUser(t *testing.T) {

	t.Run("Test Update User", func(t *testing.T) {

		fake := faker.New()
		reqBody := []byte(fmt.Sprintf(`{"username":"%s","password":"%s"}`, fake.Person().Name(), fake.Internet().Password()))
		req, err := http.NewRequest("PUT", "/update/"+"73f2fc91-d245-40c6-b15c-58d82098495b", bytes.NewBuffer(reqBody))
		if err != nil {
			t.Fatal(err)
		}

		rr := httptest.NewRecorder()

		handler, err := controller.NewHandler()
		if err != nil {
			t.Fatal(err)
		}
		handler.UpdateUser(rr, req)

		if status := rr.Code; status != 200 {
			t.Errorf("handler returned wrong status code: got %v want %v", status, 200)
		}
	})
}

func TestDeleteUser(t *testing.T) {

	t.Run("Test DELETE User", func(t *testing.T) {

		req, err := http.NewRequest("DELETE", "/delete/"+"73f2fc91-d245-40c6-b15c-58d82098495b", nil)
		if err != nil {
			t.Fatal(err)
		}

		rr := httptest.NewRecorder()

		handler, err := controller.NewHandler()
		if err != nil {
			t.Fatal(err)
		}

		handler.DeleteUser(rr, req)

		if status := rr.Code; status != 200 {
			t.Errorf("handler returned wrong status code: got %v want %v", status, 200)
		}
	})
}

func TestGetAllQuery(t *testing.T) {

	t.Run("Test GetAllQuery User", func(t *testing.T) {

		handler, err := controller.NewHandler()
		if err != nil {
			t.Fatal(err)
		}

		_, err = handler.GetAllUser()
		if err != nil {
			t.Fatal(err)
		}

	})
}

func TestInsertQuery(t *testing.T) {

	t.Run("Test GetAllQuery User", func(t *testing.T) {
		fake := faker.New()

		handler, err := controller.NewHandler()
		if err != nil {
			t.Fatal(err)
		}

		err = handler.Insert(&controller.User{
			ID:       fake.UUID().V4(),
			Username: fake.Person().FirstName(),
			Password: fake.Internet().Password(),
		})
		if err != nil {
			t.Fatal(err)
		}

	})
}

func TestUpdateQuery(t *testing.T) {

	t.Run("Test GetAllQuery User", func(t *testing.T) {
		fake := faker.New()

		handler, err := controller.NewHandler()
		if err != nil {
			t.Fatal(err)
		}

		err = handler.Update("73f2fc91-d245-40c6-b15c-58d82098495b", fake.Person().Name(), fake.Internet().Password())
		if err != nil {
			t.Fatal(err)
		}

	})
}
