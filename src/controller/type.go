package controller

type User struct {
	ID       string
	Username string `db:"username"`
	Password string `db:"password"`
}
