package controller

import (
	"database/sql"
	"encoding/json"
	"net/http"
	"strings"

	"github.com/rizkyian78/test/config"
	uuid "github.com/satori/go.uuid"
)

type Handler struct {
	db *sql.DB
}

func NewHandler() (*Handler, error) {

	db, err := config.DBInitialize("127.0.0.1", 5432, "postgres", "password", "dev")
	if err != nil {
		return &Handler{}, err
	}

	return &Handler{
		db: db,
	}, nil
}

func (h *Handler) GetUser(w http.ResponseWriter, r *http.Request) {

	if r.Method != "GET" {
		http.Error(w, "Invalid Method", 400)
		return
	}

	users, err := h.GetAllUser()
	if err != nil {
		http.Error(w, err.Error(), 400)
		return
	}
	jsonInBytes, err := json.Marshal(users)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(jsonInBytes)

	// var user []User

}

func (h *Handler) AddUser(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		http.Error(w, "Invalid Method", 400)
		return
	}

	r.Body = http.MaxBytesReader(w, r.Body, 1048576)

	dec := json.NewDecoder(r.Body)
	var p User
	err := dec.Decode(&p)
	if err != nil {
		http.Error(w, err.Error(), 400)
		return
	}

	p.ID = uuid.NewV4().String()

	err = h.Insert(&User{
		ID:       p.ID,
		Username: p.Username,
		Password: p.Password,
	})
	if err != nil {
		http.Error(w, err.Error(), 400)
		return
	}
	jsonInBytes, err := json.Marshal(p)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(jsonInBytes)
}

func (h *Handler) UpdateUser(w http.ResponseWriter, r *http.Request) {
	if r.Method != "PUT" {
		http.Error(w, "Invalid Method", 400)
		return
	}
	id := strings.TrimPrefix(r.URL.Path, "/update/")

	r.Body = http.MaxBytesReader(w, r.Body, 1048576)

	dec := json.NewDecoder(r.Body)
	var p User
	err := dec.Decode(&p)
	if err != nil {
		http.Error(w, err.Error(), 400)
		return
	}

	err = h.Update(id, p.Username, p.Password)
	if err != nil {
		http.Error(w, err.Error(), 400)
		return
	}
	jsonInBytes, err := json.Marshal(map[string]interface{}{
		"message": "Success update",
	})
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(jsonInBytes)
}

func (h *Handler) DeleteUser(w http.ResponseWriter, r *http.Request) {
	if r.Method != "DELETE" {
		http.Error(w, "Invalid Method", 400)
		return
	}
	id := strings.TrimPrefix(r.URL.Path, "/delete/")
	err := h.Delete(id)
	if err != nil {
		http.Error(w, err.Error(), 400)
		return
	}
	jsonInBytes, err := json.Marshal(map[string]interface{}{
		"message": "Success delete",
	})
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(jsonInBytes)

}

func (h *Handler) Insert(u *User) error {
	_, err := h.db.Exec(`INSERT INTO users (id,username,password) VALUES ($1,$2,$3)`, u.ID, u.Username, u.Password)
	if err != nil {
		return err
	}

	return nil
}

func (h *Handler) Delete(id string) error {
	_, err := h.db.Exec(`delete from users where id = $1`, id)
	if err != nil {
		return err
	}
	return nil
}

func (h *Handler) Update(id, username, password string) error {
	_, err := h.db.Exec(`UPDATE users set username = $1, password = $2 where id = $3`, username, password, id)
	if err != nil {
		return err
	}

	return nil
}

func (h *Handler) GetAllUser() (users []User, err error) {
	row, err := h.db.Query("SELECT * FROM users ORDER BY username ASC")
	if err != nil {
		return users, err
	}
	defer row.Close()

	for row.Next() {
		user := User{}
		if err := row.Scan(
			&user.ID,
			&user.Username,
			&user.Password); err != nil {
			return users, err
		}
		users = append(users, user)
	}

	return users, nil
}
