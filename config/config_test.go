package config_test

import (
	"testing"

	"github.com/rizkyian78/test/config"
)

func TestInitializeDB(t *testing.T) {

	t.Run("Test Get All User", func(t *testing.T) {

		host := "127.0.0.1"
		port := 5432 // replace with your test port
		user := "postgres"
		password := "password"
		dbName := "dev"

		_, err := config.DBInitialize(host, port, user, password, dbName)
		if err != nil {
			t.Fatalf("Error initializing database: %v", err)
		}
	})

}
